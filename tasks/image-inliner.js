/* eslint-disable */

var through = require('through2');
var datauri = require('datauri').sync;
var gutil = require('gulp-util');
var path = require('path');
var PluginError = gutil.PluginError;

const PLUGIN_NAME = 'Image Inliner';

module.exports = function (opts) {
  opts = opts || {};

  var regexpCss = 'url\\s*\\(\\s*["\']?([^\\)"\']+)\\?embed["\']?\\s*\\)';
  var regexpHtml = 'src=["\']([^"\']+)\\?embed["\']';

  var rInlineImage = new RegExp(regexpCss + '|' + regexpHtml, 'gi');
  
  function inlineImage(file, encoding, callback) {
    
    if (file.isNull()) {
      return callback(null, file);
    }
    
    if (file.isStream()) {
      new PluginError(PLUGIN_NAME, 'This plugin does not support streams');
    }
    
    var baseDir = opts.baseDir || file.base;
    var src = file.contents.toString();
    var match;
    var imageURI;
    
    while (match = rInlineImage.exec(src)) {
      var imagePath = match[1] || match[2];

      if (imagePath[0] === '/') {
        imagePath = imagePath.slice(1);
      }

      var fullImagePath = path.resolve(baseDir, imagePath.replace('/',path.sep));

      console.log(fullImagePath);

      imageURI = datauri(fullImagePath);
      
      src = src.replace(match[0], match[1] ? 'url("'+imageURI+'")' : 'src="'+imageURI+'"');
    }
    
    file.contents = new Buffer(src);

    return callback(null, file);
  }
  
  return through.obj(inlineImage);
};

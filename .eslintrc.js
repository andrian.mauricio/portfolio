module.exports = {
  extends: 'react-tools',
  rules: {
    'linebreak-style': [
      'error',
      'windows'
    ],
    'max-len': [
      'error',
      {
        'ignorePattern': '>.+</|alt=.+',
        'code': 100,
        'ignoreTemplateLiterals': true,
      },
    ],
  },
}

import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-static'

const Navbar = styled.nav`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-around;
  width: 100vw;

  a {
    border-bottom: 0 solid white;
    color: white;
    display: block;
    line-height: 4rem;
    margin: 1rem 0;
    transition: all .3s ease-in-out;

    &.active {
      border-bottom-width: .5rem;
      margin-bottom: .5rem;
    }
  }
`

export default () => (
  <Navbar>
    <Link to="/" exact>Inicio</Link>
    <Link to="/yo">Yo</Link>
    <Link to="/trabajos">Trabajos</Link>
    <Link to="/contacto">Contacto</Link>
  </Navbar>
)

import styled from 'styled-components'

export default styled.figure`
  height: 100%;
  margin: 0;
  overflow: hidden;
  padding: 0;
  position: relative;
  width: 100%;
`

import React from 'react'

import { addToLazyload } from './lazyload'


class Lazy extends React.Component {
  componentDidMount () {
    const { margin, callback } = this.props

    addToLazyload({
      target: this.fullImage,
      margin: margin || '300px 0px',
      callback,
    })
  }

  render () {
    const { image, type, style, alt, className } = this.props

    const Tag = `${type}`

    return (
      <Tag
        data-lazy
        data-src={image}
        ref={fullImage => { this.fullImage = fullImage }}
        alt={alt}
        style={style}
        className={className}
      />
    )
  }
}

export default Lazy

const observerConfig = margin => ({
  rootMargin: margin,
})

const setImageSrc = (elem, callback) => {
  const dataSrc = elem.getAttribute('data-src')

  if (dataSrc) {
    const image = new Image()

    image.onload = () => {
      callback(elem, image.src)
    }

    image.src = dataSrc
  }
}

const onIntersection = callback => (entries, observer) => {
  entries.forEach(entry => {
    if (entry.intersectionRatio > 0) {
      observer.unobserve(entry.target)
      setImageSrc(entry.target, callback)
    }
  })
}

export const addToLazyload = ({ target, margin, callback }) => {
  if (typeof window !== 'undefined') {
    if (!('IntersectionObserver' in window)) {
      require('intersection-observer')
    }

    const lazyloadObserver = new IntersectionObserver(
      onIntersection(callback),
      observerConfig(margin)
    )

    lazyloadObserver.observe(target)
  }
}

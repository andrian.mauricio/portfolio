import styled from 'styled-components'

export default styled.div`
  background-image: url('${({ inlineImage }) => inlineImage}?embed');
  background-size: cover;
  bottom: 0;
  filter: blur(1.5rem);
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  z-index: 1;
`

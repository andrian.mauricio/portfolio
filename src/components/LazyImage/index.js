import React from 'react'

import Figure from './Figure'
import Placeholder from './Placeholder'
import Lazy from './Lazy'

const callback = (elem, src) => {
  elem.style.opacity = '1'
  elem.style.backgroundImage = `url('${src}')`
  elem.removeAttribute('data-src')
}

const LazyImage = ({ image, inlineImage, alt, width, height, styles, position }) => (
  <Figure style={{ ...styles, width, height }}>
    <Lazy
      type="div"
      className="lazy-image"
      {...{
        image,
        alt,
        callback,
        style: {
          width,
          height,
          backgroundPosition: position,
        },
      }} />
    <Placeholder inlineImage={inlineImage} />
    <figcaption className="sr-only">{alt}</figcaption>
  </Figure>
)

export default LazyImage

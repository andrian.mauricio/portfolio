import styled from 'styled-components'

export default styled.div`
  background: transparent;
  background-size: cover;
  bottom: 0;
  left: 0;
  opacity: 0;
  position: absolute;
  right: 0;
  top: 0;
  transition: opacity 1s ease;
  z-index: 2;
`

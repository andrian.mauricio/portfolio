import styled from 'styled-components'
import { colors } from '../styles/theme'

export default styled.button`
  background-color: white;
  border-radius: .2rem;
  border: none;
  box-shadow: 0rem .3rem 1rem rgba(0, 0, 0, .25);
  color: ${colors.rgb('blue')};
  display: block;
  font-size: 1.8rem;
  margin: ${({ center, right }) => (
    center ? '0 auto' :
      right ? '0 0 0 auto' : '0 auto 0 0'
  )};
  outline: none;
  padding: 1rem 3rem;
  text-transform: uppercase;
  transition: all .3s ease-in-out;

  &:hover {
    box-shadow: 0rem .8rem 3rem rgba(0,0,0,.15);
    transform: scale(1.25);
  }

  a {
    color: inherit;
  }
`

import styled from 'styled-components'

const Header = styled.header`
  ${({ noImage }) => noImage ? 'margin-bottom: 8rem;' : ''}
  overflow: auto;

  h1 {
    color: white;
    text-align: center;
  }
`

export default Header

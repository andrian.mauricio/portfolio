import React from 'react'
import styled from 'styled-components'

const List = styled.ul`
  list-style: none;
  padding-left: 5rem;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

const Item = styled.li`
  &:hover {
    i {
      transform: translateX(11rem) translateY(.1rem) rotate(180deg);
    }
  }

  i {
    display: inline-block;
    transition: all .3s ease-in-out;
    transform: translateY(.2rem);
    margin-right: 1rem;
  }
`

export default ({ list }) => (
  <List>
    {list.map(item => (
      <Item key={item.key} {...item.options}><i>arrow</i>{item.content}</Item>
    ))}
  </List>
)


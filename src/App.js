import React from 'react'
import { Router } from 'react-static'
import { injectGlobal } from 'styled-components'
import { hot } from 'react-hot-loader'
import Routes from 'react-static-routes'

import { globalStyles, AppStyles } from './styles'

import Nav from './components/Nav'

injectGlobal`${globalStyles}`

const App = () => (
  <Router>
    <AppStyles>
      <Nav />
      <main>
        <Routes />
      </main>
    </AppStyles>
  </Router>
)

export default hot(module)(App)

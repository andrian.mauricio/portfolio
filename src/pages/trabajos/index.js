import React from 'react'
import styled from 'styled-components'

import Header from '../../components/Header'
import LazyImage from '../../components/LazyImage'
import Cta from '../../components/Cta'
import Separator from './Separator'

const Section = styled.section`
  padding: 0 3rem 5rem;

  > p, > h1, > h2 {
    margin-left: 0;
    margin-right: 0;
  }
`

const shadow = {
  boxShadow: '0rem .3rem 1rem rgba(0, 0, 0, .25)',
}

export default () => (
  <div>
    <Header noImage>
      <h1>Mis Trabajos</h1>
    </Header>
    <Section>
      <h2>Kinesic</h2>
      <LazyImage
        image="/images/logo-kinesic.jpeg"
        inlineImage="/images/logo-kinesic-lqip.jpeg"
        alt="Logo de Kinesic"
        width="100%"
        height="21rem"
        position="left top"
        styles={shadow} />
      <p>Kinesic es un centro de salud kinesiológico ubicado en la ciudad de Resistencia, Chaco.</p>
      <Cta right><a href="https://kinesicweb.net/" target="_blank" rel="noopener noreferrer">Visitar Web</a></Cta>
      <Separator top="4" />
      <p>Su principal labor es brindar un servicio de salud óptimo. Para lograrlo, además de contratar excelentes profesionales, decidieron expendirse a la web y facilitar a sus pacientes un acceso más rápido a sus números de telefonos y dirección postal.</p>
      <LazyImage
        image="/images/kinesic-page.jpeg"
        inlineImage="/images/kinesic-page-lqip.jpeg"
        alt="Captura de pantalla de la página web de Kinesic"
        width="100%"
        height="20rem"
        position="left top"
        styles={shadow} />
      <Separator />
      <p>Su página web cuenta con un botón que presionado en el teléfono celular, abre una conversación de <strong>Whatsapp</strong> con la persona seleccionada como destinatario. De la misma manera ocurre con el botón <strong>Llamar</strong>.</p>
      <LazyImage
        image="/images/kinesic-botones.jpeg"
        inlineImage="/images/kinesic-botones-lqip.jpeg"
        alt="Captura de pantalla de la funcionalidad de los botones Whatsapp y Llamar de la página web de Kinesic"
        width="100%"
        height="20rem"
        styles={shadow} />
      <Separator />
      <p>Además, la página muestra un mapa con la ubicación del centro de kinesiología para que sus pacientes actuales y futuros puedan entender su ubicación con mucha más facilidad.</p>
      <LazyImage
        image="/images/kinesic-mapa.jpeg"
        inlineImage="/images/kinesic-mapa-lqip.jpeg"
        alt="Captura de pantalla de la funcionalidad de mapa de la página web de Kinesic"
        width="100%"
        height="24rem"
        position="center top"
        styles={shadow} />
      <Separator />
      <p>Una funcionalidad única que acompaña a la sección <strong>Obras Sociales</strong> es la posibilidad de buscar cualquier obra social de la lista. Si esta aparece, significa que Kinesic tiene convenio con dicha obra social. La búsqueda se realiza automáticamente con gran velocidad a medida que el usuario tipea, sin necesidad de esperar a ningún servidor para que procese dicha búsqueda.</p>
      <LazyImage
        image="/images/kinesic-obras-sociales.jpeg"
        inlineImage="/images/kinesic-obras-sociales-lqip.jpeg"
        alt="Captura de pantalla de la funcionalidad de búsqueda de Obras Sociales de la página web de Kinesic"
        width="100%"
        height="38rem"
        position="center top"
        styles={shadow} />
    </Section>
  </div>
)

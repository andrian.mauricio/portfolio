import styled from 'styled-components'
import { colors } from '../../styles/theme'

export default styled.hr`
  width: 6.5rem;
  height: .1rem;
  background-color: ${colors.rgb('blue')};
  display: block;
  margin: ${({ top }) => top || '5'}rem auto ${({ bottom }) => bottom || '4'}rem 0;
  border: none;
`

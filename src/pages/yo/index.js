import React from 'react'

import Header from '../../components/Header'
import ProfileImage from './ProfileImage'
import LazyImage from '../../components/LazyImage'
import List from '../../components/List'

export default () => (
  <div>
    <Header>
      <h1>Sobre Mi</h1>
    </Header>
    <section>
      <ProfileImage>
        <LazyImage
          image="/images/perfil.jpeg"
          inlineImage="/images/perfil-lqip.jpeg"
          alt="Imágen de perfil"
          width="11rem"
          height="11rem" />
      </ProfileImage>
      <h2>Soy Mauricio Andrian y me dedico al diseño web.</h2>
      <p>Me dedico principalmente al diseño visual de páginas webs (Frontend): animaciones, colores, formas, tipografía, contenido, etcétera.</p>
      <p>Mis herramientas pueden variar dependiendo del tamaño del proyecto. Uso principalmente HTML, CSS y Javascript pero también he trabajo en anteriores proyectos con React, AngularJS y Angular 2+. Por ejemplo, esta página está hecha principalmente con React y React Static.</p>
      <p>Es también parte de mi trabajo lograr que la página web sea encontrada por Google y sea mostrada en los buscadores de la mejor manera posible. Mi objetivo es que el proyecto también adopte las técnicas necesarias para que se le facilite la interacción entre la página web y las distintas redes sociales como Whatsapp, Facebook, Instagram, Twitter, Google+, entre otras.</p>
      <p>También soy un aficionado del diseño audio visual, y me apasiona tomar fotografías profesionales y editar videos de alta calidad.</p>
      <p>Todas estas habilidades se pueden combinar para poder ayudarte a vos y a tu empresa o producto a resaltar de entre los demás.</p>
    </section>
    <section>
      <h2>Lenguajes que uso</h2>
      <List list={[
        { content: 'HTML5', key: 'HTML5' },
        { content: 'CSS3', key: 'CSS3' },
        { content: 'Javascript', key: 'Javascript' },
      ]} />
    </section>
    <section>
      <h2>Tecnologías que conozco</h2>
      <List list={[
        { content: 'React', key: 'React' },
        { content: 'Angular 2+', key: 'Angular 2+' },
        { content: 'AngularJS', key: 'AngularJS' },
        { content: 'RxJS', key: 'RxJS' },
        { content: 'Vue 2', key: 'Vue 2' },
        { content: 'Ember', key: 'Ember' },
      ]} />
    </section>
  </div>
)

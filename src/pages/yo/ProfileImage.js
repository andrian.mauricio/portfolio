import styled from 'styled-components'

export default styled.div`
width: 12rem;
height: 12rem;
margin: 0 auto;
overflow: hidden;
border-radius: 100%;
border: .5rem solid white;
box-shadow: 0 .5rem 1.5rem rgba(0,0,0,.25);
`

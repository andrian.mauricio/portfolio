import styled from 'styled-components'
import baseStyles from './baseStyle'

export default styled.figure`
  ${baseStyles}

  i {
    &:first-child {
      font-size: 13em;
      left: 18%;
      top: 43%;
      z-index: 2;
    }

    &:nth-child(2) {
      font-size: 10em;
      left: 1%;
      top: 12%;
      z-index: 1;
    }

    &:last-child {
      font-size: 10em;
      left: 57%;
      top: 18%;
      z-index: 0;
    }
  }
`

import styled from 'styled-components'
import baseStyles from './baseStyle'

export default styled.figure`
  ${baseStyles}

  i {
    &:first-child {
      font-size: 15em;
      left: 4%;
      top: 35%;
      z-index: 2;
    }

    &:nth-child(2) {
      font-size: 9em;
      left: 25%;
      top: 3%;
      z-index: 1;
    }

    &:last-child {
      font-size: 7em;
      left: 62%;
      top: 47%;
      z-index: 0;
    }
  }
`


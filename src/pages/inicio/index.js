import React from 'react'
import { Link } from 'react-static'

import Header from '../../components/Header'
import Cta from '../../components/Cta'
import Section from './Section'
import Conoceme from './Conoceme'
import Trabajos from './Trabajos'
import Contacto from './Contacto'

/**
 * @typedef {Object} Section
 * @property {any} Component
 * @property {string[]} icons
 * @property {string} CTA
 */

/** @type {Section[]} */
const datos = [
  {
    Component: Conoceme,
    href: '/yo',
    icons: ['handshake', 'card', 'user'],
    ctaText: 'Conoceme',
  },
  {
    Component: Trabajos,
    href: '/trabajos',
    icons: ['briefcase', 'filetext', 'filecode'],
    ctaText: 'Mis Trabajos',
  },
  {
    Component: Contacto,
    href: '/contacto',
    icons: ['phone', 'whatsapp', 'facebook'],
    ctaText: 'Contacto',
  },
]

export default () => (
  <div>
    <Header noImage>
      <h1>Inicio</h1>
    </Header>
    { datos.map(({ Component, icons, ctaText, href }) => (
      <Section key={ctaText}>
        <Component>
          {icons.map(icon => <i key={icon}>{icon}</i>)}
        </Component>
        <Cta center><Link to={href}>{ctaText}</Link></Cta>
      </Section>
    ))}
  </div>
)

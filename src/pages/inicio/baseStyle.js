import { css } from 'styled-components'
import { colors } from '../../styles/theme'

export default css`
display: block;
font-size: .625em;
height: 20em;
margin: 0 auto 2rem;
position: relative;
width: 20em;

i {
  display: block;
  position: absolute;
  line-height: 1em;
  background: -webkit-linear-gradient(-45deg, ${colors.rgb('green')}, ${colors.rgb('blue')});
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  &:first-child {
    color: ${colors.rgb('blue')};
  }

  &:last-child {
    color: ${colors.rgb('green')};
  }
}
`

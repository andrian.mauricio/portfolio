import styled from 'styled-components'

export default styled.section`
  padding: 3rem 0;

  h2 {
    text-align: center;
  }
`

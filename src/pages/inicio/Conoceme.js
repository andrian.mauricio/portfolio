import styled from 'styled-components'
import baseStyles from './baseStyle'

export default styled.figure`
  ${baseStyles}

  i {
    &:first-child {
      font-size: 11em;
      left: 16%;
      top: 47%;
      z-index: 2;
    }

    &:nth-child(2) {
      font-size: 9em;
      left: 49%;
      top: 12%;
      z-index: 1;
    }

    &:last-child {
      font-size: 9em;
      left: 1%;
      line-height: 11rem;
      top: 9%;
      z-index: 0;
    }
  }
`

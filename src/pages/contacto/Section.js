import styled from 'styled-components'
import { colors } from '../../styles/theme'

export default styled.section`
  p > a {
    color: ${colors.rgb('blue')};
    text-decoration: underline;
  }
`

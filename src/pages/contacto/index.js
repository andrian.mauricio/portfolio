import React from 'react'

import Header from '../../components/Header'
import Cta from '../../components/Cta'
import Section from './Section'

const data = [
  {
    icon: 'gmail',
    service: 'Email',
    info: 'andrian.mauricio@gmail.com',
    href: 'mailto:',
  },
  {
    icon: 'facebook',
    service: 'Facebook',
    info: '/mauricio.andrian',
    href: 'https://www.facebook.com',
  },
  {
    icon: 'linkedin',
    service: 'LinkedIn',
    info: '/mauricio-andrian-4973b0114',
    href: 'https://www.linkedin.com/in',
  },
  {
    icon: 'github',
    service: 'GitHub',
    info: '/AndrianMauricio',
    href: 'https://github.com',
  },
]

export default () => (
  <div>
    <Header noImage>
      <h1>Contacto</h1>
    </Header>
    {data.map(({ icon, info, service, href }) => (
      <Section key={service}>
        <h2><i className={`i-${icon}`} /> {service}:</h2>
        {href && <p><a href={`${href}${info}`}>{info} <i>open</i></a></p>}
      </Section>
    ))}
    <Section>
      <h2><i className="i-phone" /> Teléfono:</h2>
      <p><Cta><a href="https://api.whatsapp.com/send?phone=5493794689294">Whatsapp</a> <i className="i-whatsapp" /></Cta></p>
      <p><Cta><a href="tel:5493794689294">Llamar</a> <i className="i-phone" style={{ color: '#2274e7' }} /></Cta></p>
    </Section>
  </div>
)

import React from 'react'
import Header from '../components/Header'

export default () => (
  <div>
    <Header>
      <h1>404 - Oh no's! We couldn't find that page :(</h1>
    </Header>
  </div>
)

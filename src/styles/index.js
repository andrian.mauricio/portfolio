import styled from 'styled-components'
import { theme } from './theme'

const globalStyles = `
  @font-face {
    font-family: 'mauricio';
    src:
      url('/fonts/mauricio.ttf?6m4i9q') format('truetype'),
      url('/fonts/mauricio.woff?6m4i9q') format('woff'),
      url('/fonts/mauricio.svg?6m4i9q#mauricio') format('svg');
    font-weight: normal;
    font-style: normal;
  }

  @font-face {
    font-family: 'mauricio';
    src:
      url('/fonts/mauricio.ttf?fn8kwp') format('truetype'),
      url('/fonts/mauricio.woff?fn8kwp') format('woff'),
      url('/fonts/mauricio.svg?fn8kwp#mauricio') format('svg');
    font-weight: normal;
    font-style: normal;
  }

  i, .icomoon-liga {
    /* use !important to prevent issues with browser extensions that change fonts */
    font-family: 'mauricio' !important;
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;

    /* Enable Ligatures ================ */
    letter-spacing: 0;
    -webkit-font-feature-settings: "liga";
    -moz-font-feature-settings: "liga=1";
    -moz-font-feature-settings: "liga";
    -ms-font-feature-settings: "liga" 1;
    font-feature-settings: "liga";
    -webkit-font-variant-ligatures: discretionary-ligatures;
    font-variant-ligatures: discretionary-ligatures;

    /* Better Font Rendering =========== */
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  .i-facebook:before {
    content: "\\e900";
    color: #3b5998;
  }
  .i-github:before {
    content: "\\e901";
  }
  .i-gmail:before {
    content: "\\e902";
    color: #d14836;
  }
  .i-linkedin:before {
    content: "\\e903";
    color: #0077b5;
  }
  .i-whatsapp:before {
    content: "\\e904";
    color: #25d366;
  }
  .i-open:before {
    content: "\\e89e";
  }
  .i-arrow:before {
    content: "\\f054";
  }
  .i-phone:before {
    content: "\\f095";
  }
  .i-briefcase:before {
    content: "\\f0b1";
  }
  .i-envelope:before {
    content: "\\f0e0";
  }
  .i-terminal:before {
    content: "\\f120";
  }
  .i-code:before {
    content: "\\f121";
  }
  .i-filetext:before {
    content: "\\f15c";
  }
  .i-filecode:before {
    content: "\\f1c9";
  }
  .i-frames:before {
    content: "\\f248";
  }
  .i-handshake:before {
    content: "\\f2b5";
  }
  .i-card:before {
    content: "\\f2bc";
  }
  .i-user:before {
    content: "\\f2be";
  }
  .i-license:before {
    content: "\\f2c3";
  }

  * {
    box-sizing: border-box;
  }

  html {
    font-size: 10px;
    line-height: 1.5;
    -webkit-text-size-adjust: 100%;
  }

  body {
    font-family: 'Open Sans', -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
    font-size: 1.6rem;
    margin: 0;
    padding: 0;
    background-image: url('/images/bg-pattern.jpeg');
  }

  .bg-header {
    position: absolute;
    background-image: ${theme.gradient}, url(/images/cover-image.jpeg);
    z-index: 0;
    width: 100vw;
    height: 21rem;
    background-size: auto, cover;
    background-position: center;
  }

  #root {
    position: absolute;
    z-index: 1;
  }

  main {
    overflow: auto;
  }

  h1 {
    font-size: 2em;
    margin: 0.67em 0;
    text-align: center;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  p {
    margin-left: 3rem;
    margin-right: 3rem;
  }

  .sr-only {
    position: absolute;
    width: 1px;
    height: 1px;
    padding: 0;
    margin: -1px;
    overflow: hidden;
    clip: rect(0,0,0,0);
    border: 0;
  }

  .lazy-image {
    background-image: none;
    background-size: cover;
    background-position: center;
    opacity: 0;
    position: absolute;
    transition: opacity 1s ease;
    z-index: 2;
  }

  hr {
    box-sizing: content-box;
    height: 0;
    overflow: visible;
  }

  pre {
    font-family: monospace, monospace;
    font-size: 1em;
  }

  a {
    background-color: transparent;
    text-decoration: none;
    color: black;
  }

  abbr[title] {
    border-bottom: none;
    text-decoration: underline;
    text-decoration: underline dotted;
  }

  b,
  strong {
    font-weight: bolder;
  }

  code,
  kbd,
  samp {
    font-family: monospace, monospace;
    font-size: 1em;
  }

  small {
    font-size: 80%;
  }

  sub,
  sup {
    font-size: 75%;
    line-height: 0;
    position: relative;
    vertical-align: baseline;
  }

  sub {
    bottom: -0.25em;
  }

  sup {
    top: -0.5em;
  }

  img {
    border-style: none;
  }

  button,
  input,
  optgroup,
  select,
  textarea {
    font-family: inherit;
    font-size: 100%;
    line-height: 1.15;
    margin: 0;
  }

  button,
  input {
    overflow: visible;
  }

  button,
  select {
    text-transform: none;
  }

  button,
  [type="button"],
  [type="reset"],
  [type="submit"] {
    -webkit-appearance: button;
  }

  button::-moz-focus-inner,
  [type="button"]::-moz-focus-inner,
  [type="reset"]::-moz-focus-inner,
  [type="submit"]::-moz-focus-inner {
    border-style: none;
    padding: 0;
  }

  button:-moz-focusring,
  [type="button"]:-moz-focusring,
  [type="reset"]:-moz-focusring,
  [type="submit"]:-moz-focusring {
    outline: 1px dotted ButtonText;
  }

  fieldset {
    padding: 0.35em 0.75em 0.625em;
  }

  legend {
    box-sizing: border-box;
    color: inherit;
    display: table;
    max-width: 100%;
    padding: 0;
    white-space: normal;
  }

  progress {
    vertical-align: baseline;
  }

  textarea {
    overflow: auto;
  }

  [type="checkbox"],
  [type="radio"] {
    box-sizing: border-box;
    padding: 0;
  }

  [type="number"]::-webkit-inner-spin-button,
  [type="number"]::-webkit-outer-spin-button {
    height: auto;
  }

  [type="search"] {
    -webkit-appearance: textfield;
    outline-offset: -2px;
  }

  [type="search"]::-webkit-search-decoration {
    -webkit-appearance: none;
  }

  ::-webkit-file-upload-button {
    -webkit-appearance: button;
    font: inherit;
  }

  details {
    display: block;
  }

  summary {
    display: list-item;
  }

  template {
    display: none;
  }

  [hidden] {
    display: none;
  }
`
const AppStyles = styled.div``

export { globalStyles, AppStyles }

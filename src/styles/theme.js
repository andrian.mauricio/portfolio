class Colors {
  constructor () {
    this.green = { r: 0, g: 242, b: 96 }
    this.blue = { r: 34, g: 116, b: 231 }
    this.whatsapp = { r: 37, g: 211, b: 102 }
    this.facebook = { r: 59, g: 89, b: 152 }
    this.get = this.rgba.bind(this)
  }

  rgba (color, alpha = 1) {
    const { r, g, b } = this[color]
    return `rgba(${r}, ${g}, ${b}, ${alpha})`
  }

  rgb (color) {
    const { r, g, b } = this[color]
    return `rgb(${r}, ${g}, ${b})`
  }
}

const colors = new Colors()

const theme = {
  green: colors.rgb('green'),
  blue: colors.rgb('blue'),
  whatsapp: colors.rgb('whatsapp'),
  facebook: colors.rgb('whatsapp'),
  gradient: `linear-gradient(135deg, ${colors.rgba('green', 0.85)}, ${colors.rgba('blue', 0.85)})`,
}

export { theme, colors }

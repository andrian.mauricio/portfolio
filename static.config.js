import React, { Component } from 'react'
import { ServerStyleSheet } from 'styled-components'

export default {
  getRoutes: async () => [
    {
      path: '/',
      component: 'src/pages/inicio',
    },
    {
      path: '/yo',
      component: 'src/pages/yo',
    },
    {
      path: '/trabajos',
      component: 'src/pages/trabajos',
    },
    {
      path: '/contacto',
      component: 'src/pages/contacto',
    },
    {
      is404: true,
      component: 'src/pages/404',
    },
  ],
  devServer: {
    port: 4000,
    host: '127.0.0.1',
  },
  siteRoot: process.env.NODE_ENV === 'production' ?
    'https://mauricio.netlify.com' :
    'https://localhost:4000',
  renderToHtml: (render, Comp, meta) => {
    const sheet = new ServerStyleSheet()
    const html = render(sheet.collectStyles(<Comp />))
    meta.styleTags = sheet.getStyleElement()
    return html
  },
  Document: class CustomHtml extends Component {
    render () {
      const { Html, Head, Body, children, renderMeta } = this.props

      return (
        <Html>
          <Head>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            {renderMeta.styleTags}
          </Head>
          <Body><div className="bg-header" />{children}</Body>
        </Html>
      )
    }
  },
}

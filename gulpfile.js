/* eslint-disable */

var gulp          = require('gulp');
var rename        = require('gulp-rename');
var imagemin      = require('gulp-imagemin');
var imageResize   = require('gulp-image-resize');
var clean         = require('gulp-clean');
var webp          = require('gulp-webp');
var imageInline   = require('./tasks/image-inliner');

var config = {
  maxSize: 1920,
  lqipSize: 48,
  originFolder: 'src/images',
  destFolder: 'public/images',
  outputFormat: 'jpeg',
  webp: false,
}

gulp.task('clean', function () {
  return gulp.src(config.destFolder, { read: false })
    .pipe(clean());
})

gulp.task('max-size', ['clean'], function () {
  return gulp.src(config.originFolder + '/**/*.{png,gif,jpg,jpeg,webp}')
    .pipe(imageResize({ width : config.maxSize, format: config.outputFormat }))
    .pipe(rename({ dirname: '' }))
    .pipe(gulp.dest(config.destFolder));
});

gulp.task('lqip', ['clean'], function () {
  return gulp.src(config.originFolder + '/**/*.{png,gif,jpg,jpeg,webp}')
    .pipe(imageResize({ width : config.lqipSize, format: config.outputFormat }))
    .pipe(rename({ suffix: '-lqip', dirname: '' }))
    .pipe(gulp.dest(config.destFolder));
});

gulp.task('minimize', ['max-size', 'lqip'], function () {
  return gulp.src(config.destFolder + '/*.{jpg,jpeg}')
    .pipe(imagemin({ progressive: true, verbose: true }))
    .pipe(gulp.dest(config.destFolder));
});

if (config.webp) {
  gulp.task('webp', ['minimize'], function () {
    return gulp.src(config.destFolder + '/*.{jpg,jpeg}')
      .pipe(webp())
      .pipe(gulp.dest(config.destFolder));
  });
}

gulp.task('process-images', [config.webp ? 'webp' : 'minimize']);

gulp.task('inline-images', function () {
  return gulp.src('dist/**/*.html')
    .pipe(imageInline())
    .pipe(gulp.dest('dist'));
});
